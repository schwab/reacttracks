/**
 * @jsx React.DOM
 */

'use strict';

var React = require('react');
var DefaultLayout = require('../layouts/DefaultLayout.jsx');
var ReactTracks = require('../components/ReactTracks.jsx');

var HomePage = React.createClass({
  getDefaultProps() {
    return {
      title: 'ReactTracks demo application',
      layout: DefaultLayout
    };
  },
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-sm-offset-2">
            <ReactTracks />
          </div>
        </div>
      </div>
    );
  }
});

module.exports = HomePage;
