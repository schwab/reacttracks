var AppDispatcher = require('../AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var ActionTypes = require('../constants/ActionTypes');
var assign = require('object-assign');
var API = require('../utils/Api');

var CHANGE_EVENT = 'change',
    tracks = [], tracksTotal, filter = {}, sort, currentPage = 1, offset = 0, limit = 30;

function loadTracks() {
    API.getTracks(filter, sort, offset, limit);
}

function processTracksData(_tracks) {

    tracks = tracks.concat(_tracks.data);
    tracksTotal = _tracks.total;

    ReactTracksStore.emitChange();
}

function updateFilter(_filter) {

    resetTracksData();
    filter = _filter;
    nextPage();
}

function updateSortOptions(sortField) {

    resetTracksData();
    sort = sortField;
    nextPage();
}

function resetTracksData() {

    tracks = [];
    offset = 0;
    currentPage = 1;
}

function nextPage(number) {

    offset = (currentPage-1)*limit;
    currentPage++;
    loadTracks();
}

var ReactTracksStore = assign({}, EventEmitter.prototype, {

    getTracks: function () {
        return tracks;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (payload) {

    var action = payload.action;

    switch (action.actionType) {
        case ActionTypes.ReactTracks.CHANGE_SORT:
            updateSortOptions(action.sort);
            break;

        case ActionTypes.ReactTracks.CHANGE_FILTER:
            updateFilter(action.filter);
            break;

        case ActionTypes.ReactTracks.REQUEST_TRACKS_SUCCESS:
            processTracksData(action.tracks);
            break;

        case ActionTypes.ReactTracks.INIT:
            loadTracks();
            break;

        case ActionTypes.ReactTracks.LOAD_NEXT_PAGE:
            nextPage(action.page);
            break;
    }
});

module.exports = ReactTracksStore;
