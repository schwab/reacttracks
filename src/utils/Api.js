
var ReactTracksActions = require('../actions/ReactTracksActions');
var jQuery = require('jquery');
var assign = require('object-assign');

var API_BASE_URL = 'http://localhost:8888';

function get(path, params) {
   return jQuery.getJSON(API_BASE_URL + path, params);
}

var API = {

    getTracks(filter, sort, offset, limit) {

        var params = {};
        assign(params, filter);

        if(sort) {
            params.sort = sort;
        }

        if(offset) {
            params.offset = offset;
        }

        if(limit) {
            params.q = limit;
        }

        return get('/tracks', params)
            .done(function(response) {
                ReactTracksActions.requestTracksSuccess(response);
            })
            .fail(function(response) {
                console.log('Something wrong!')
            })
    }
};

module.exports = API;
