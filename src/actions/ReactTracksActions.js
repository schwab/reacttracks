'use strict';

var AppDispatcher = require('../AppDispatcher');
var ActionTypes = require('../constants/ActionTypes');

var ReactTracksActions = {

    init() {
        AppDispatcher.handleViewAction({
            actionType: ActionTypes.ReactTracks.INIT
        })
    },

    changeFilter(filter) {
        AppDispatcher.handleViewAction({
            actionType: ActionTypes.ReactTracks.CHANGE_FILTER,
            filter : filter
        });
    },

    changeSort(sort) {
        AppDispatcher.handleViewAction({
            actionType: ActionTypes.ReactTracks.CHANGE_SORT,
            sort: sort
        });
    },

    requestTracksSuccess(response) {
        AppDispatcher.handleServerAction({
            actionType: ActionTypes.ReactTracks.REQUEST_TRACKS_SUCCESS,
            tracks: response
        });
    },

    loadNextPage(number) {
        if(AppDispatcher.isDispatching()) return;

        AppDispatcher.handleViewAction({
            actionType: ActionTypes.ReactTracks.LOAD_NEXT_PAGE,
            page: number
        });
    }
};

module.exports = ReactTracksActions;
