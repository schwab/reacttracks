/**
 * @jsx React.DOM
 */

'use strict';

var React = require('react');
var ReactTracksStore = require('../stores/ReactTracksStore');
var ReactTracksActions = require('../actions/ReactTracksActions');
var InfiniteScroll = require('react-infinite-scroll-mixin');


function getState() {
    return {
        tracks: ReactTracksStore.getTracks()
    };
}

var ReactTracks = React.createClass({

    mixins: [InfiniteScroll],

    getInitialState() {
        return getState();
    },

    componentDidMount: function() {

        ReactTracksStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {

        ReactTracksStore.removeChangeListener(this._onChange);
    },

    render() {

        var tracks = [];
        this.state.tracks.forEach(function(trackData, key){
            tracks.push(<Track key={trackData.id} trackData={trackData} />);
        });

        return (
            <div>
                <FilterBar />
                <table className="table">
                    <ListHeader />
                    <tbody>
                    {tracks}
                    </tbody>
                </table>
            </div>
        );
    },

    _onChange() {
        this.setState(getState());
    },

    fetchNextPage() {
        ReactTracksActions.loadNextPage();
    }
});

var FilterBar = React.createClass({

    handleChange() {

        var filter = {
            artist: this.refs.filterArtistInput.getDOMNode().value,
            title: this.refs.filterTitleInput.getDOMNode().value
        };

        ReactTracksActions.changeFilter(filter)
    },

    render() {
        return (
            <div className="well">
            <div className="row">
                <div className="col-sm-6">
                    <input type="text"
                        className="form-control"
                        ref="filterArtistInput"
                        onChange={this.handleChange}
                        placeholder="Artist" />
                </div>
                <div className="col-sm-6">
                    <input type="text"
                        className="form-control"
                        ref="filterTitleInput"
                        onChange={this.handleChange}
                        placeholder="Track title" />
                </div>
            </div>
            </div>
        )
    }
});


var ListHeader = React.createClass({

    handleSortChange(field) {
        ReactTracksActions.changeSort(field);
    },

    render() {
        return (
            <thead>
                <tr>
                    <th className="col-sm-4">Artist</th>
                    <th className="col-sm-4">Track title</th>
                    <th className="col-sm-2">
                        <a href="#" onClick={this.handleSortChange.bind(this, 'genre')}>Genre</a>
                    </th>
                    <th className="col-sm-2">
                        <a href="#" onClick={this.handleSortChange.bind(this, 'duration')}>Duration</a>
                    </th>
                </tr>
            </thead>
        );
    }
});

var Track = React.createClass({

    formatDuration(seconds) {

        return Math.floor(seconds/60) + ':' + ((seconds%60 < 10)? '0': '') + seconds%60;
    },

    render() {
        return (
            <tr>
                <td>{this.props.trackData.artist}</td>
                <td>{this.props.trackData.title}</td>
                <td>{this.props.trackData.genre}</td>
                <td>{this.formatDuration(this.props.trackData.duration)}</td>
            </tr>
        );
    }
});

module.exports = ReactTracks;
