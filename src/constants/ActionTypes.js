module.exports = {

  // Route action types
  SET_CURRENT_ROUTE: 'SET_CURRENT_ROUTE',

  // Reacttracks action types
  ReactTracks: {
      CHANGE_FILTER: 'CHANGE_FILTER',
      CHANGE_SORT: 'CHANGE_SORT',
      REQUEST_TRACKS_SUCCESS: 'REQUEST_TRACKS_SUCCESS',
      LOAD_NEXT_PAGE: 'LOAD_NEXT_PAGE'
  }
};
