/**
 * @jsx React.DOM
 */

'use strict';

var React = require('react');
var Link = require('../components/Link.jsx');

var DefaultLayout = React.createClass({
  propTypes: {
    title: React.PropTypes.string,
    breadcrumb: React.PropTypes.component
  },
  getDefaultProps() {
    return {
      title: 'ReactTracks demo application',
      description: 'Built with React.js and Flux'
    };
  },
  render() {

    return (
      <div>
        <div className="navbar-top">
          <div className="container">
            <div className="navbar-brand">
              <img src="/images/logo-small.png" width="38" height="38" alt="React" />
              <span>ReactTracks</span>
            </div>
          </div>
        </div>
        {this.props.children}
        <div className="navbar-footer">
          <div className="container">
            <p className="text-muted">
            </p>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = DefaultLayout;
