<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

$tracksDb = require_once(__DIR__ . '/../resources/tracksDb.php');

$app->get('/tracks', function (Request $request) use ($app, $tracksDb) {
    $allowedSortFields = ['duration', 'genre', 'title'];
    $maxLimit = 50;
    $defaultLimit = 20;

    $filter = [
        'title' => $request->query->get('title', null),
        'artist' => $request->query->get('artist', null)
    ];

    $sortBy = $request->query->get('sort', null);
    $sortBy = in_array($sortBy, $allowedSortFields)?$sortBy:null;

    $limit = min($request->query->get('q', $defaultLimit), $maxLimit);

    $offset = $request->query->get('offset', 0);

    $result = from($tracksDb)
        ->where(function($t)  use ($filter) {
                return !$filter['title'] || strpos(strtolower($t['title']), strtolower($filter['title'])) !== false;
        })
        ->where(function($t) use ($filter) {
                return !$filter['artist'] || strpos(strtolower($t['artist']), strtolower($filter['artist'])) !== false;
        });

    if($sortBy) {
        $result = $result->orderBy(
            function($t) use ($sortBy) {return $t[$sortBy]; },
            function($t1, $t2) { return strnatcasecmp($t1, $t2);}
        );
    }

    return  $app->json([
        'data' => $result->skip($offset)->take($limit)->toList(),
        'total' => $result->count(),
        'limit' => $limit,
        'offset' => $offset
    ]);
});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html'
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
});
